<?php


namespace App\Exception;


class ContentException extends \Exception
{
    public function __construct(string $message = 'Ошибка содержимого', $code = 400)
    {
        parent::__construct($message, $code);
    }
}
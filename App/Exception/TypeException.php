<?php


namespace App\Exception;



class TypeException extends \Exception
{
    public function __construct(string $type, $code = 400)
    {
        parent::__construct(sprintf("Данный тип %s не поддерживается!", $type), $code);
    }
}
<?php


namespace App\LongArithmetic;


interface ILongArithmetic
{
    /** @return array */
    public function getNumberToArray(): array;

    /**
     * @param array $number
     */
    public function setNumber(array $number);
}
<?php


namespace App\LongArithmetic;


abstract class LongArithmetic implements ILongArithmetic
{
    protected const DIVISOR = 10;

    /**
     * @param ILongArithmetic $number
     */
    public function addSum(ILongArithmetic $number)
    {
        $firstLength = count($this->getNumberToArray());
        $secondLength = count($number->getNumberToArray());

        $maxLength = max($firstLength, $secondLength);

        $firstNumber = array_reverse($this->getNumberToArray());
        $secondNumber = array_reverse($number->getNumberToArray());

        $result = [];
        $amountDigits = 0;

        for ($index = 0; $index < $maxLength || $amountDigits > 0; $index++) {
            if ($index < $firstLength) {
                $amountDigits += (int)$firstNumber[$index];
            }

            if ($index < $secondLength) {
                $amountDigits += (int)$secondNumber[$index];
            }

            array_push($result, $amountDigits % self::DIVISOR);

            $amountDigits = intdiv($amountDigits, self::DIVISOR);
        }

        return $this->setNumber(array_reverse($result));
    }

    /**
     * @param array $number
     */
    abstract public function setNumber(array $number);

    /** @return array */
    abstract public function getNumberToArray(): array;
}
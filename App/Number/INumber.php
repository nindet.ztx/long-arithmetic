<?php


namespace App\Number;


interface INumber
{
    /** @param string|int|array $parameter */
    public function __construct($parameter);

    /** @return array */
    public function getNumberToArray(): array;

    /**
     * @param array $number
     * @return INumber
     */
    public function setNumber(array $number): INumber;

    /** @return string */
    public function getNumberToString(): string;
}
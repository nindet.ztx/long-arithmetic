<?php


namespace App\Number\Implementation;


use App\LongArithmetic\LongArithmetic;
use App\Exception\ContentException;
use App\Exception\TypeException;
use App\Number\INumber;

class Number extends LongArithmetic implements INumber
{
    protected const TYPE_STRING = 'string';
    protected const TYPE_ARRAY = 'array';
    protected const TYPE_INTEGER = 'integer';

    /** @var array */
    protected array $number = [];

    public function __construct($parameter)
    {
        $this->number = $this->convertToUniversalType($parameter);
    }

    /** @return array */
    public function getNumberToArray(): array
    {
        return $this->number;
    }

    /** @return string */
    public function getNumberToString(): string
    {
        return implode('', $this->number);
    }

    /**
     * @param array $number
     * @return INumber
     */
    public function setNumber(array $number): INumber
    {
        $this->number = $number;
        return $this;
    }

    /** @param string|int|array $parameter
     * @return array
     * @throws TypeException
     */
    protected function convertToUniversalType($parameter): array
    {
        switch (gettype($parameter)) {
            case self::TYPE_INTEGER:
                return $this->convertToArray(strval($parameter));
            case self::TYPE_ARRAY:
                return $this->convertToArray(implode( '', $parameter));
            case self::TYPE_STRING:
                return $this->convertToArray($parameter);
            default:
                throw new TypeException(gettype($parameter));
        }
    }

    protected function convertToArray(string $number): array
    {
        $this->validation($number);

        if (ltrim($number, '0') === '') {
            $number = '0';
        }

        return str_split($number);
    }

    protected function validation(string $number): void
    {
        if (!ctype_digit($number)) {
            throw new ContentException();
        }

        if (strlen($number) === 0) {
            throw new ContentException('Параметр не должен быть пустой');
        }
    }
}
<?php

use App\Number\Implementation\Number;


require __DIR__.'/vendor/autoload.php';

$numberOne = new Number('3333336666666666678888888888833333333333');
$numberTwo = new Number(['3333336666666666678888888888833333333333']);

var_dump($numberOne->getNumberToString());
var_dump($numberTwo->getNumberToString());
$numberOne->addSum($numberTwo);
var_dump($numberOne->getNumberToString());
